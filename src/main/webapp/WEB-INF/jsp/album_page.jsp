<%--
  Created by IntelliJ IDEA.
  User: huotaol
  Date: 2017/11/28
  Time: 10:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core_rt" %>
<html>
    <head>
        <title>相册</title>
        <meta charset="utf-8"/>
        <link rel="stylesheet" type="text/css" href="<c:url value="/resource/css/style.css"/> "/>
        <script src="<c:url value="/resource/js/move.js"/>" type="text/javascript"></script>
        <script src="<c:url value="/resource/js/index.js"/>" type="text/javascript"></script>
    </head>
    <body>

        <div class="wrapper">

        </div>

        <div id="pxs_container" class="pxs_container">
            <div class="pxs_bg">
                <div class="pxs_bg1"></div>
                <div class="pxs_bg2"></div>
                <div class="pxs_bg3"></div>
            </div>
            <div class="pxs_loading">Loading images...</div>
            <div class="pxs_slider_wrapper">
                <ul class="pxs_slider">
                    <c:forEach items="${pageBean.list()}" var="fileData">
                        <li>
                            <img width="759px" height="280px"
                                 src="<c:url value="/image/item?fileName=${fileData.url}&width=759&height=280"/>"/>
                        </li>
                    </c:forEach>
                </ul>
                <div class="pxs_navigation">
                    <span class="pxs_next"></span>
                    <span class="pxs_prev"></span>
                </div>
                <ul class="pxs_thumbnails">
                    <c:forEach items="${pageBean.list()}" var="fileData">
                        <li>
                            <img width="95px" height="65px"
                                 src="<c:url value="/image/item?fileName=${fileData.url}&width=95&height=65"/>"/>"
                        </li>
                    </c:forEach>
                </ul>
            </div>
        </div>
        <script src="<c:url value="/resource/js/load.js"/>"></script>
    </body>
</html>
