package com.huotao.service.impl;

import com.huotao.domain.bean.ResultBean;
import com.huotao.domain.convet.UserConvertImpl;
import com.huotao.domain.dto.UserDTO;
import com.huotao.domain.entity.UserEntity;
import com.huotao.service.IUserService;
import com.huotao.util.JWT;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 项目名称：CaoHai
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/11/15 17:07
 * <br>修改人:
 * <br>修改时间：2017/11/15 17:07
 * <br>修改备注：
 *
 * @author htliu
 * @date 2017/11/15 17:07
 */
@Service
public class UserServiceImpl extends BaseService implements IUserService {

    @Resource
    private UserConvertImpl userConvert;

    @Override
    public ResultBean<UserDTO> login(UserDTO userDTO) {
        UserEntity userEntity = find("from UserEntity where name=?", userDTO.getName());
        checkData(userEntity, "没有该用户");
        if (!encode(userDTO.getPassword()).equals(userEntity.getPassword())) {
            throw new IllegalArgumentException("密码错误");
        }
        userDTO.setPassword(userEntity.getPassword());
        String token = JWT.sign(userDTO, 60 * 1000 * 60 * 2L);
        userDTO.setToken(token);
        return new ResultBean<UserDTO>().data(userDTO);
    }

    @Override
    public ResultBean addUser(UserDTO userDTO) {
        UserEntity userEntity = userConvert.reverse(userDTO);
        userEntity.setPassword(encode(userEntity.getPassword()));
        addData(userEntity);
        return new ResultBean();
    }

}
