package com.huotao.service.impl;

import com.huotao.dao.BaseDao;
import com.huotao.domain.bean.IEntity;
import com.huotao.util.FileUtil;
import com.huotao.util.ImageUtil;
import lombok.Data;
import lombok.experimental.Delegate;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 项目名称：CaiHai
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/9/15 10:16
 * <br>修改人:
 * <br>修改时间：2017/9/15 10:16
 * <br>修改备注：
 * <br>@version
 */
@Service
@Data
public class BaseService {
    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());
    @Resource
    @Delegate
    protected BaseDao baseDao;
    @Value("${config.page_size}")
    protected int pageSize;
    @Resource
    protected ImageUtil imageUtil;
    @Resource
    protected FileUtil fileUtil;
    @Value("${config.pic_base_url}")
    protected String basePicUrl;
    @Value("${config.pic_path}")
    protected String baseLocalPic;
    @Value("${config.salt}")
    protected String salt;

    public void addData(IEntity data) {
        Date date = new Date();
        data.setCreateTime(date);
        data.setModifyTime(date);
        baseDao.add(data);
    }

    public void update(IEntity data) {
        data.setModifyTime(new Date());
//        baseDao.update(data);
    }

    /**
     * @param entity
     * @param ignores {@link #wrapIgnores(String...)} 默认包含该参数
     * @return 数据库持久化对象
     */
    protected <T extends IEntity> T addOrUpdate(IEntity entity, String... ignores) {
        IEntity data = baseDao.find(entity.getClass(), entity.getId());
        if (data == null) {
            addData(entity);
            return (T) entity;
        } else {
            BeanUtils.copyProperties(entity, data, wrapIgnores(ignores));
            update(data);
            return (T) data;
        }
    }

    /**
     * 检查该参数是否有效，无效的话，抛出统一异常
     *
     * @param data
     * @param messages
     */
    protected void checkData(Object data, String... messages) {
        if (data == null) {
            throw new IllegalArgumentException(messages.length > 0 ? messages[0] : "");
        }
    }


    protected String[] wrapIgnores(String... params) {
        List<String> list = new ArrayList<>();
        list.add("id");
        list.add("createTime");
        list.add("modifyTime");
        list.addAll(Arrays.asList(params));
        String[] data = new String[list.size()];
        return list.toArray(data);
    }

    protected long getCountByHql(String hql, Object... params) {
        List list = baseDao.listByHql("select count(*) " + hql, params);
        return Long.parseLong(list.get(0).toString());
    }

    protected long getCountBySql(String sql, Object... params) {
        List list = baseDao.listBySql(sql, params);
        return Long.parseLong(list.get(0).toString());
    }

    protected String encode(String content, String salt) {
        return encode(content, salt);
    }

    protected String encode(String content) {
        Md5Hash md5Hash = new Md5Hash(content, salt);
        return md5Hash.toString();
    }
}
