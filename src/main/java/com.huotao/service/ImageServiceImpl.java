package com.huotao.service;

import com.huotao.domain.bean.PageBean;
import com.huotao.domain.bean.ResultBean;
import com.huotao.domain.entity.FileDataEntity;
import com.huotao.service.impl.BaseService;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * 项目名称：AlbumDemo
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/11/27 14:49
 * <br>修改人:
 * <br>修改时间：2017/11/27 14:49
 * <br>修改备注：
 *
 * @author htliu
 * @date 2017/11/27 14:49
 */
@Service
public class ImageServiceImpl extends BaseService implements IImageService {
    @Override
    public ResultBean addIamges(MultipartFile[] files) {
        if (files.length == 0) {
            return new ResultBean().status(1).message("至少有一张图片");
        }
        for (MultipartFile file : files) {
            FileDataEntity large = new FileDataEntity();
            large.setUrl(imageUtil.savePic(imageUtil.getLargeWidth(), imageUtil.getLargeHeight(), file));
            addData(large);
        }
        return new ResultBean().message("添加成功");

    }

    @Override
    public void getImage(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html; charset=UTF-8");
        response.setContentType("image/jpeg");
        String fileName = request.getParameter("fileName");
        if (StringUtils.isEmpty(fileName)) {
            throw new IllegalArgumentException("没有图片名");
        }
        int width = Optional.ofNullable(request.getParameter("width"))
                .filter(s -> !StringUtils.isEmpty(s))
                .map(Integer::valueOf).orElseGet(() -> imageUtil.getLargeWidth());
        int height = Optional.ofNullable(request.getParameter("height"))
                .filter(s -> !StringUtils.isEmpty(s))
                .map(Integer::valueOf)
                .orElseGet(() -> imageUtil.getLargeHeight());

        String newpath = new String(fileName.getBytes("ISO-8859-1"), "UTF-8");
        String absolutePath = baseLocalPic + File.separator + newpath;
        OutputStream os = response.getOutputStream();
        try {
            BufferedImage finalImage = Thumbnails.of(absolutePath)
                    .size(width, height)
                    .imageType(BufferedImage.TYPE_INT_ARGB)
                    .asBufferedImage();
            ImageIO.write(finalImage, "png", os);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (os != null) {
                os.close();
            }
        }
    }

    @Override
    public ResultBean deleteImage(long id) {
        delete(FileDataEntity.class, id);
        return new ResultBean().message("删除图片成功");
    }

    @Override
    public ResultBean listImage(int page, int pageSize) {
        PageBean<FileDataEntity> pageBean = listPageImage(page, pageSize);
        Map<String, Object> map = new HashMap(2);
        map.put("baseUrl", basePicUrl);
        map.put("data", pageBean);
        return new ResultBean<>().data(map);
    }

    @Override
    public PageBean<FileDataEntity> listPageImage(int page, int pageSize) {
        String hql = "from FileDataEntity";
        List<FileDataEntity> list = listPageByHql(hql, page, pageSize);
        long count = getCountByHql(hql);
        PageBean<FileDataEntity> pageBean = new PageBean<>();
        pageBean.current(page).total(count).list(list);
        return pageBean;
    }
}
