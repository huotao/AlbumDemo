package com.huotao.service;

import com.huotao.service.impl.BaseService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 项目名称：CaiHai
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/9/6 14:28
 * <br>修改人:
 * <br>修改时间：2017/9/6 14:28
 * <br>修改备注：
 * <br>@version
 */
@Service
public class AuthorizationService extends BaseService {

    public List<String> listRoles(String userName) {
        String sql = "SELECT r.RoleName FROM re_user_role rur JOIN USER u ON rur.UserID = u.UserID AND u.Username = ? JOIN role r ON rur.RoleID = r.RoleID";
        List list = listBySql(sql, userName);
        return list;
    }

    public List<String> listPermissions(String roleName) {
        String sql = "SELECT p.Permissionname FROM re_role_permission rrp JOIN permission p ON rrp.PeID = p.PeID JOIN role r ON rrp.RoleID = r.RoleID and r.Rolename=?";
        List list = listBySql(sql, roleName);
        return list;
    }

}
