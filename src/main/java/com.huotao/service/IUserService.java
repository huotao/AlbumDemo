package com.huotao.service;


import com.huotao.domain.bean.ResultBean;
import com.huotao.domain.dto.UserDTO;

/**
 * 项目名称：CaoHai
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/11/15 17:05
 * <br>修改人:
 * <br>修改时间：2017/11/15 17:05
 * <br>修改备注：
 *
 * @author htliu
 * @date 2017/11/15 17:05
 */
public interface IUserService {

    /**
     * 登录
     *
     * @param userDTO
     * @return
     */
    ResultBean<UserDTO> login(UserDTO userDTO);

    /**
     * 添加用户
     *
     * @param userDTO
     * @return
     */
    ResultBean addUser(UserDTO userDTO);
}
