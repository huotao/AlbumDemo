package com.huotao.service;

import com.huotao.domain.bean.PageBean;
import com.huotao.domain.bean.ResultBean;
import com.huotao.domain.entity.FileDataEntity;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 项目名称：AlbumDemo
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/11/27 14:39
 * <br>修改人:
 * <br>修改时间：2017/11/27 14:39
 * <br>修改备注：
 *
 * @author htliu
 * @date 2017/11/27 14:39
 */
public interface IImageService {


    /**
     * 添加图片，压缩三份
     *
     * @param files
     * @return
     */
    ResultBean addIamges(MultipartFile[] files);

    void getImage(HttpServletRequest request, HttpServletResponse response) throws IOException;

    /**
     * 删除图片
     *
     * @param id
     */
    ResultBean deleteImage(long id);

    /**
     * @param page
     * @param pageSize
     * @return
     */
    ResultBean listImage(int page, int pageSize);

    /**
     * @param page
     * @param pageSize
     * @return
     */
    PageBean<FileDataEntity> listPageImage(int page, int pageSize);
}
