package com.huotao.util;

import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

/**
 * 项目名称：CaoHai
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/11/16 11:47
 * <br>修改人:
 * <br>修改时间：2017/11/16 11:47
 * <br>修改备注：
 *
 * @author htliu
 * @date 2017/11/16 11:47
 */
@Component
public class Util {
    /**
     * 检查参数,未通过直接抛出异常
     *
     * @param result
     * @throws IllegalArgumentException
     */
    public void checkResult(BindingResult result) throws IllegalArgumentException {
        if (result.hasErrors()) {
            String resultStr = result.getFieldErrors().stream()
                    .map(v -> v.getField() + " " + v.getDefaultMessage())
                    .reduce((a, b) -> a + "  " + b).orElse("");
            throw new IllegalArgumentException(resultStr);
        }
    }
}
