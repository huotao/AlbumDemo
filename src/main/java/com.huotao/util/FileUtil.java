package com.huotao.util;

import com.huotao.domain.bean.FileType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

/**
 * 项目名称：CaiHai
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/10/9 14:30
 * <br>修改人:
 * <br>修改时间：2017/10/9 14:30
 * <br>修改备注：
 * <br>@version
 */
@Component
public class FileUtil {
    @Value("${config.pic_path}")
    protected String basePath;

    /**
     * @param file
     * @return null failed
     */
    public String save(MultipartFile file) {
        String fileName = generateFileName(file);
        try {
            file.transferTo(new File(basePath + File.separator + fileName));
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        return fileName;
    }


    protected String generateFileName(MultipartFile file) {
        String postFix = "";
        try {
            FileType fileType = getType(file.getInputStream());
            postFix = fileType.getValue().toLowerCase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        postFix = StringUtils.isEmpty(postFix) ? "" : ("." + postFix);
        return UUID.randomUUID().toString() + postFix;
    }


    /**
     * 返回文件类型
     *
     * @param filePath 文件在该主机中的存储地址
     * @return null 没有该文件类型
     */
    public FileType getType(String filePath) {
        return getFileType(getFileHeader(filePath));
    }

    /**
     * 返回文件类型
     *
     * @param inputStream
     * @return null 没有该文件类型
     */
    public FileType getType(InputStream inputStream) {
        return getFileType(getFileHeader(inputStream));
    }

    /**
     * 根据文件头返回文件类型
     *
     * @param fileHead
     * @return null 没有该文件类型
     */
    private FileType getFileType(String fileHead) {
        if (fileHead != null && fileHead.length() > 0) {
            fileHead = fileHead.toUpperCase();
            FileType[] fileTypes = FileType.values();
            for (FileType type : fileTypes) {
                if (fileHead.startsWith(type.getValue())) {
                    return type;
                }
            }
        }
        return null;
    }


    /**
     * 读取文件头
     *
     * @param filePath 文件在该主机中的存储地址
     * @return
     */
    private String getFileHeader(String filePath) {
        try {
            return bytesToHex(getBytes(new FileInputStream(filePath)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 读取文件头
     */
    private String getFileHeader(InputStream inputStream) {
        try {
            return bytesToHex(getBytes(inputStream));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    private byte[] getBytes(InputStream inputStream) throws IOException {
        byte[] b = new byte[28];
        try {
            inputStream.read(b, 0, 28);
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
        }
        return b;
    }


    /**
     * 将字节数组转换成16进制字符串
     */
    private String bytesToHex(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }


}
