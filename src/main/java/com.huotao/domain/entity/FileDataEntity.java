package com.huotao.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.huotao.domain.bean.IEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 项目名称：YiDianCan
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/9/15 14:43
 * <br>修改人:
 * <br>修改时间：2017/9/15 14:43
 * <br>修改备注：
 * <br>@version
 */
@Data
@EqualsAndHashCode(of = "id")
@Entity
@Table
@Accessors(chain = true)
public class FileDataEntity implements IEntity, Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private long id;

    @Column
    private String name;

    @Column
    private String alias;

    @Column
    private String description;

    @Column(unique = true, nullable = false)
    private String url;

    @Column
    @Temporal(value = TemporalType.TIMESTAMP)
    @JsonIgnore
    private Date createTime;

    @Column
    @Temporal(value = TemporalType.TIMESTAMP)
    @JsonIgnore
    private Date modifyTime;
}
