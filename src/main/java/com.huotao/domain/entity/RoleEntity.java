package com.huotao.domain.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.huotao.domain.bean.IEntity;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * 项目名称：AlbumDemo
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/11/27 13:26
 * <br>修改人:
 * <br>修改时间：2017/11/27 13:26
 * <br>修改备注：
 *
 * @author htliu
 * @date 2017/11/27 13:26
 */
@Entity
@Table
@Data
@Accessors(chain = true)
public class RoleEntity implements Serializable, IEntity {
    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue
    private long id;

    @Column
    private String name;

    @Column
    @Temporal(value = TemporalType.TIMESTAMP)
    @JsonIgnore
    private Date createTime;

    @Column
    @Temporal(value = TemporalType.TIMESTAMP)
    @JsonIgnore
    private Date modifyTime;
}
