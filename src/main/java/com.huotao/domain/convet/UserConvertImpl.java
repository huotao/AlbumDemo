package com.huotao.domain.convet;

import com.huotao.domain.bean.IConvert;
import com.huotao.domain.dto.UserDTO;
import com.huotao.domain.entity.UserEntity;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

/**
 * 项目名称：AlbumDemo
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/11/27 13:33
 * <br>修改人:
 * <br>修改时间：2017/11/27 13:33
 * <br>修改备注：
 *
 * @author htliu
 * @date 2017/11/27 13:33
 */
@Component
public class UserConvertImpl implements IConvert<UserEntity, UserDTO> {
    @Override
    public UserDTO convert(UserEntity userEntity) {
        UserDTO userDTO = new UserDTO();
        BeanUtils.copyProperties(userEntity, userDTO);
        return userDTO;
    }

    @Override
    public UserEntity reverse(UserDTO userDTO) {
        UserEntity userEntity = new UserEntity();
        BeanUtils.copyProperties(userDTO, userEntity);
        return userEntity;
    }
}
