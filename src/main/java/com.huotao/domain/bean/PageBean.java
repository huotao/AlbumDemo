package com.huotao.domain.bean;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

/**
 * 项目名称：CaoHai
 * <br>类描述：统一分页数据封装
 * <br>创建人：htliu
 * <br>创建时间：2017/11/17 13:48
 * <br>修改人:
 * <br>修改时间：2017/11/17 13:48
 * <br>修改备注：
 *
 * @author htliu
 * @date 2017/11/17 13:48
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@Data
@Accessors(fluent = true)
public class PageBean<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    private long total;
    private int current;
    private List<T> list;
}
