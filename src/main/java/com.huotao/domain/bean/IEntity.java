package com.huotao.domain.bean;

import java.util.Date;

/**
 * 项目名称：CaiHai
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/9/27 14:03
 * <br>修改人:
 * <br>修改时间：2017/9/27 14:03
 * <br>修改备注：
 * <br>@version
 */
public interface IEntity {

    long getId();

    Object setCreateTime(Date date);

    Object setModifyTime(Date date);
}
