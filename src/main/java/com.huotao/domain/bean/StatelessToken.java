package com.huotao.domain.bean;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.apache.shiro.authc.AuthenticationToken;

/**
 * 项目名称：CaiHai
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/9/19 10:57
 * <br>修改人:
 * <br>修改时间：2017/9/19 10:57
 * <br>修改备注：
 * <br>@version
 */
@Data
@AllArgsConstructor
public class StatelessToken implements AuthenticationToken {
    private String username;
    private String clientDigest;
    //省略部分代码
    @Override
    public Object getPrincipal() {  return username;}
    @Override
    public Object getCredentials() {  return clientDigest;}
}