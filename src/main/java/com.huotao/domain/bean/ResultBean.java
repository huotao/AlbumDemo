package com.huotao.domain.bean;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 项目名称：CaiHai
 * <br>类描述：统一返回数据
 * <br>创建人：htliu
 * <br>创建时间：2017/9/15 16:49
 * <br>修改人:
 * <br>修改时间：2017/9/15 16:49
 * <br>修改备注：
 * <br>@version
 */
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
@Data
@Accessors(fluent = true)
public class ResultBean<T> implements Serializable {
    public static final int SUCCESS = 0;
    public static final int FAILED = 1;
    /**
     * 该用户没有相关权限
     */
    public static final int NO_PERMISSION = 2;
    private static final long serialVersionUID = 1L;
    /**
     * 默认设置 SUCCESS
     */
    private int status = SUCCESS;
    private String message = "SUCCESS";
    private T data;

}
