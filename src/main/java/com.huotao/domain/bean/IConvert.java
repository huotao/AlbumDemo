package com.huotao.domain.bean;

/**
 * 项目名称：CaiHai
 * <br>类描述：bean对象转换、回退
 * <br>创建人：htliu
 * <br>创建时间：2017/11/1 9:26
 * <br>修改人:
 * <br>修改时间：2017/11/1 9:26
 * <br>修改备注：
 *
 * @param <T> 当前bean
 * @param <E> 需要转换的bean
 * @author htliu
 */
public interface IConvert<T, E> {

    E convert(T t);

    T reverse(E e);
}
