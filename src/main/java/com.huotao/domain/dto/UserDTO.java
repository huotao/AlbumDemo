package com.huotao.domain.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.NotBlank;

/**
 * 项目名称：CaoHai
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/11/15 17:54
 * <br>修改人:
 * <br>修改时间：2017/11/15 17:54
 * <br>修改备注：
 *
 * @author htliu
 * @date 2017/11/15 17:54
 */
@Data
@Accessors(chain = true)
public class UserDTO {
    @NotBlank
    private String name;

    @NotBlank
    private String password;

    private String token;

}
