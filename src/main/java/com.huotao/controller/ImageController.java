package com.huotao.controller;

import com.huotao.domain.bean.ResultBean;
import com.huotao.service.IImageService;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 项目名称：AlbumDemo
 * <br>类描述：图片处理
 * <br>创建人：htliu
 * <br>创建时间：2017/11/27 14:38
 * <br>修改人:
 * <br>修改时间：2017/11/27 14:38
 * <br>修改备注：
 *
 * @author htliu
 * @date 2017/11/27 14:38
 */
@RestController
@RequestMapping("/image")
public class ImageController {
    @Resource
    private IImageService imageService;

    @PostMapping("/items")
    public ResultBean addIamges(@RequestParam("file") MultipartFile[] files) {
        return imageService.addIamges(files);
    }

    @GetMapping("/item")
    public void getImage(HttpServletRequest request, HttpServletResponse response) throws IOException {
        imageService.getImage(request, response);
    }

    @DeleteMapping("item/{id}")
    public ResultBean deleteImage(@PathVariable long id) {
        return imageService.deleteImage(id);
    }

    @GetMapping("/items")
    public ResultBean listImages(@RequestParam("page") int page, @RequestParam("pageSize") int pageSize) {
        return imageService.listImage(page, pageSize);
    }

}
