package com.huotao.controller;

import com.huotao.domain.bean.PageBean;
import com.huotao.domain.entity.FileDataEntity;
import com.huotao.service.IImageService;
import com.huotao.service.impl.BaseService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * 项目名称：AlbumDemo
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/11/28 10:17
 * <br>修改人:
 * <br>修改时间：2017/11/28 10:17
 * <br>修改备注：
 *
 * @author htliu
 * @date 2017/11/28 10:17
 */
@Controller
@RequestMapping("/album")
public class AlbumController {
    @Resource
    private IImageService imageService;

    @Resource
    private BaseService baseService;

    @RequestMapping("/show")
    public String showAlbum(Model model) {
        PageBean<FileDataEntity> pageBean = imageService.listPageImage(1, 100);
        model.addAttribute("baseUrl", baseService.getBasePicUrl());
        model.addAttribute("pageBean", pageBean);
        return "album_page";
    }
}
