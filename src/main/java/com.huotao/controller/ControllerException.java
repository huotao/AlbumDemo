package com.huotao.controller;

import com.huotao.domain.bean.ResultBean;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;

/**
 * 项目名称：YiDianCan
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/9/27 15:19
 * <br>修改人:
 * <br>修改时间：2017/9/27 15:19
 * <br>修改备注：
 * <br>@version
 */
@RestControllerAdvice
public class ControllerException {
    @ExceptionHandler(value = IllegalArgumentException.class)
    public Object handler(HttpServletRequest request, Exception ex) {
        ex.printStackTrace();
        String message = ex.getMessage();
        return new ResultBean().status(101).message(StringUtils.isEmpty(message) ? "参数错误" : message);
    }

}
