package com.huotao.controller;

import com.huotao.domain.bean.ResultBean;
import com.huotao.domain.dto.UserDTO;
import com.huotao.service.IUserService;
import com.huotao.util.Util;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 项目名称：CaoHai
 * <br>类描述：
 * <br>创建人：htliu
 * <br>创建时间：2017/11/15 17:08
 * <br>修改人:
 * <br>修改时间：2017/11/15 17:08
 * <br>修改备注：
 *
 * @author htliu
 * @date 2017/11/15 17:08
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @Resource
    private IUserService userService;
    @Resource
    private Util util;

    @PostMapping("/login")
    public ResultBean login(@Valid @RequestBody UserDTO userDTO, BindingResult result) {
        util.checkResult(result);
        return userService.login(userDTO);
    }

    @PostMapping("/item")
    public ResultBean register(@Valid @RequestBody UserDTO userDTO, BindingResult result) {
        util.checkResult(result);
        return userService.addUser(userDTO);
    }

}
